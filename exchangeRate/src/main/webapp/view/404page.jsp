<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 01.08.2019
  Time: 8:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>404 Page</title>
</head>

<body>
<div align="center">
    <h1>
        Oops!
    </h1>
    <br>
    Page not found. Please, check URL.
    <br>
    <a href="/exchangeRate" target="_self">Main page</a>
</div>
</body>
</html>
