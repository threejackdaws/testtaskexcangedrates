<%@ page import="com.vmart.controller.entities.CurrencyType" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: user
  Date: 01.08.2019
  Time: 22:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Exchange rate calculation</title>
</head>
<body>

<h1 align="center"> Exchange Rate Application </h1>
<br>
<br>

<h3 align="center"> List of currency:
    <br>

    <form method="get">
        <select name="orderCurrently">

            <% List<CurrencyType> currensyOfDB = (List<CurrencyType>) request.getAttribute("listOfDB");
                for (CurrencyType currency : currensyOfDB) {
            %>

            <option value="<%=currency.getCurrencyKey()%>">
                <%= currency.getCurrencyKey() + " " + currency.getNameCurrency()%>
            </option>

            <%
                }
            %>

        </select>

        <br>
        <br>

        <button type="submit">Find exchange Rate</button>

    </form>

</h3>

<h3 align="center">

    <% if (session.getAttribute("exchangeRate") == null) {
    %>
    Please, choice currency

    <% } else {
    %>

    Desired course:
    <br>
    <%= session.getAttribute("exchangeRate")%>
    <br>
    Current time:
    <br>
    <% out.print(session.getAttribute("currentTime"));
    }
    %>

</h3>

</body>
</html>
