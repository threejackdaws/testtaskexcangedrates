package com.vmart.controller.servlets;

import com.vmart.controller.entities.CurrencyType;
import com.vmart.controller.entities.ExchangeRates;
import com.vmart.model.ModelCurrency;
import com.vmart.model.ModelExchangeRate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExchangeRate extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        getCurrencyFromDB(req);

        if (req.getParameter("orderCurrently") != null) {

            String currencyKey = req.getParameter("orderCurrently");
            Map<String, ExchangeRates> mapOfExchangeRate = ModelExchangeRate.getInstance().getMapOfExchangeRate();
            ExchangeRates currentExchangeRate = mapOfExchangeRate.get(currencyKey);

            req.getSession().setAttribute("exchangeRate", currentExchangeRate.getExchangeRate());
            req.getSession().setAttribute("currentTime", currentExchangeRate.getDate());
        }

        req.getRequestDispatcher("view/exchangeRate.jsp").forward(req, resp);
    }

    private void getCurrencyFromDB(HttpServletRequest req) {

        ModelExchangeRate.getInstance().getMapOfExchangeRate();
        Map<String, CurrencyType> model = ModelCurrency.getInstance().getMapOfCurrency();
        List<CurrencyType> listOfDB = new ArrayList<CurrencyType>();

        for (Map.Entry<String, CurrencyType> map : model.entrySet()) {
            CurrencyType currentCurrency = map.getValue();
            listOfDB.add(currentCurrency);
        }
        req.setAttribute("listOfDB", listOfDB);
    }
}
