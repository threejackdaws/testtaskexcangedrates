package com.vmart.controller.entities;

public class CurrencyType {

    private int id;
    private String currencyKey;
    private String nameCurrency;

    public CurrencyType(int id, String currencyKey, String nameCurrency) {

        this.id = id;
        this.currencyKey = currencyKey;
        this.nameCurrency = nameCurrency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurrencyKey() {
        return currencyKey;
    }

    public void setCurrencyKey(String currencyKey) {
        this.currencyKey = currencyKey;
    }

    public String getNameCurrency() {
        return nameCurrency;
    }

    public void setNameCurrency(String nameCurrency) {
        this.nameCurrency = nameCurrency;
    }
}
