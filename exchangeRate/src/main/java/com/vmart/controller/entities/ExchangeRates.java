package com.vmart.controller.entities;


public class ExchangeRates {

    private String date;
    private double exchangeRate;

    public ExchangeRates(String date, double exchangeRate) {

        this.date = date;
        this.exchangeRate = exchangeRate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(long exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
