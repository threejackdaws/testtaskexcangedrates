package com.vmart.model;

import com.vmart.controller.entities.ExchangeRates;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ModelExchangeRate {


    private static ModelExchangeRate instance = new ModelExchangeRate();

    private Map<String, ExchangeRates> mapOfExchangeRate;

    public static ModelExchangeRate getInstance() {
        return instance;
    }

    private ModelExchangeRate() {
        mapOfExchangeRate = new HashMap<String, ExchangeRates>();
        setMapOfExchangeRate();
    }

    private void setMapOfExchangeRate() {

        Instant instant = Instant.now();
        ZonedDateTime zonedDateTime = instant.atZone(ZoneId.of("Asia/Tokyo"));
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime time = zonedDateTime.toLocalDateTime();
        String currentTime = format.format(time);

        mapOfExchangeRate.put("USD", new ExchangeRates(currentTime, 15.24));
        mapOfExchangeRate.put("EUR", new ExchangeRates(currentTime, 34.75));
        mapOfExchangeRate.put("GBP", new ExchangeRates(currentTime, 47.79));
        mapOfExchangeRate.put("JPY", new ExchangeRates(currentTime, 14.45));
        mapOfExchangeRate.put("CHF", new ExchangeRates(currentTime, 24.78));
        mapOfExchangeRate.put("CNY", new ExchangeRates(currentTime, 34.73));
        mapOfExchangeRate.put("RUB", new ExchangeRates(currentTime, 57.13));
    }



    public Map<String, ExchangeRates> getMapOfExchangeRate() {
        return mapOfExchangeRate;
    }

}
