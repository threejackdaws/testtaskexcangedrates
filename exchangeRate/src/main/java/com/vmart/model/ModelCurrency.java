package com.vmart.model;

import com.vmart.controller.entities.CurrencyType;

import java.util.HashMap;
import java.util.Map;

public class ModelCurrency {

    private static ModelCurrency instance = new ModelCurrency();

    private Map<String, CurrencyType> mapOfCurrency;
    public static ModelCurrency getInstance() {
        return instance;
    }

    private ModelCurrency() {
        mapOfCurrency = new HashMap<String, CurrencyType>();
        setMapOfCurrency();
    }

    private void setMapOfCurrency() {

        mapOfCurrency.put("USD", new CurrencyType(1, "USD", "Dollar U.S."));
        mapOfCurrency.put("EUR", new CurrencyType(2, "EUR", "Euro"));
        mapOfCurrency.put("GBP", new CurrencyType(3, "GBP", "Pound sterling G.B."));
        mapOfCurrency.put("JPY", new CurrencyType(4, "JPY", "Yen"));
        mapOfCurrency.put("CHF", new CurrencyType(5, "CHF", "Swiss frank"));
        mapOfCurrency.put("CNY", new CurrencyType(6, "CNY", "Chinese Yuan Renminbi"));
        mapOfCurrency.put("RUB", new CurrencyType(7, "RUB", "Russian ruble"));

    }

    public Map<String, CurrencyType> getMapOfCurrency() {
        return mapOfCurrency;
    }
}
